## MiniX9, Morten and Line 

_OBS: Please be aware that the program takes a long time to load and that you may need to reload to get the native language on top of the image :)_

[Click to run the program](https://line_marie_dissing.gitlab.io/aesthetic-programming/miniX9)


[Link to see the code](https://gitlab.com/Line_Marie_Dissing/aesthetic-programming/-/blob/master/miniX9/index.html)
_(the button is made in the HTML-file)_


![](miniX9.png)

---

**What is the program about? Which API have you used and why?**
The program is simply a search bar where if you search for a specific country the program outputs a whole lot of information about the mother tongue of the country. We used the API [Rest countries](https://restcountries.eu/#api-endpoints-all) which is an API that contains a chunk of information about practically every country on earth. The information is stuff like name, population, currency, language and much more. This information is part of a big JSON file that our program gets the information it displays from.

---

**Can you describe and reflect on your process in this miniX in terms of Acquiring, processing, using, and representing data?**

At first we wanted to create a similar program where a user could search for a place and get the place’s longitude and latitude. We then sought some help with this and were introduced to the API we are now using. This API has a lot more data compared to only longitude and latitude and it also does not require an API key. We liked that it didn’t use a key, since the key tracks how the API is used and therefore the data the user willingly or unwillingly gives it.
Our program can through the API connect to the Rest Countries JSON file with all the data. We have then written some code that fetches data from a specific array location in the JSON file. We have then written code that displays said data on the canvas

On one hand we understand the data that is part of the JSON file. On the other hand we do not know about how the API operates or what more data is part of it than the JSON file. We also don’t know where this data comes from. Who has collected the data? Do they have biases or agendas around this data? There is a lot that we don’t know about the API, hidden in a black box.

APIs are used throughout a lot of digital culture. APIs are used so that a product or service can communicate with another product or service. This communication spans from JustEat that uses APIs to request data from restaurants to show and sell food, to internal data requesting in big corporations.
APIs also makes it possible for people to create programs that they otherwise could not create:

> “The major differences when using JSON between this and the previous chapter is that the JSON file is not located on your computer and created by yourself, but online. As such, the data is generated dynamically in (near) real-time. The JSON file has a more complex data and organizational structure.” (Soon and Cox 2020, p. 191)

Like the one we have created here, but this also comes with a caveat:

> “Although Google has provided the API to access the data, it should be remembered that the amount is limited to 100 free API requests for all units from business to non-profit organizations, and the actual data is collected from the public, and people have no access to the specific algorithm which selects, prioritizes, includes/excludes and presents the data. This raises serious questions about the degree of openness, transparency, accessibility, and inclusivity of API practices.” (Soon and Cox 2020, p. 200)

Here we can also see how APIs have power relations, where the user often does not have full access or maybe aren’t aware of internal biases or agendas of the API.

---

**Try to formulate a question in relation to web APIs or querying/parsing processes that you would like to investigate further if you had more time.**
We think it would be interesting to further investigate how biases, agenda, culture and the like of the owners and creators of the APIs come into existence in the API itself.

---

### References 
[Rest Countries](https://restcountries.eu/#api-endpoints-all) 

[Daniel Shiffmann code, 10.6: API Query with User Input](https://github.com/CodingTrain/website/blob/main/Tutorials/P5JS/p5.js/10/10.06_p5.js_api_query_user_input/sketch.js)

[Daniel Shiffmann HTML, 10.6: API Query with User Input](https://github.com/CodingTrain/website/blob/main/Tutorials/P5JS/p5.js/10/10.06_p5.js_api_query_user_input/index.html) 

[Daniel Shiffmann, 10.6: API Query with User Input](https://www.youtube.com/watch?v=4UoUqnjUC2c) 

Soon Winnie & Cox, Geoff, "Que(e)ry data", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 186-210
