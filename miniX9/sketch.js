let data2;
let input;
let api = "https://restcountries.eu/rest/v2/name/";
let showData;
let backgoundImage;

function preload(){
  backgoundImage = loadImage("worldmap.png");
}



//let url = "https://www.googleapis.com/customsearch/v1?";
//let apiKey = 'AIzaSyA6brsEgUGvGU0bGpYXPkwTqzwuUDQx1Yw';
//let units = ''&units=metric';


function setup() {
  createCanvas(800,400);



  let button = select('#submit');
  button.mousePressed(placeEarth);

  input = select('#country');

  placeEarth();



}

function placeEarth() {
  //let url = api + input.value() + apiKey + units
  let country = input.value(); //+ data2[0].languages[0].nativeName;
  let url = api + country;
  //loadJSON("https://restcountries.eu/rest/v2/name/" + country, handleData);
  showData = loadJSON(url, handleData);
}

function handleData(data) {
data2 = data;
console.log(data);



}

function draw(){
  background(255);
  background(backgoundImage);
  textAlign(CENTER);
  textFont("Times New Roman");
  textSize(75);
  text(data2[0].languages[0].nativeName, width/2, 200);

}
