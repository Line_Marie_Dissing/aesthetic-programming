
function setup(){
  createCanvas(windowWidth,windowHeight);
  frameRate(10);
  strokeWeight(3);

}

function draw(){
  background(255);
  //emoji 1
  fill(245,227,125); //yellow
  ellipse(300,300,300,300); //head

  //left eye:
  fill(255);
  ellipse(240,270,90,90);
  fill(0);
  ellipse(random(220,260),(random(290,250)),30,30); //left eye pupil
  fill(0);
  line(190,225,240,210); //eyebrow

  //right eye:
  fill(255);
  ellipse(360,270,90,90);
  fill(0);
  ellipse(random(340,390),random(250,290),30,30); //right eye pupil
  fill(0);
  line(350,210,400,225);

  //mouth
  fill(245,53,91); //pink
  arc(300,360,150,90,0,PI); //semi circel



  // emoji 2
  fill(245,227,125);
  ellipse(700,300,300,300); //head

  if (mouseIsPressed){
    fill(random(230),75,100);
    ellipse(700,375,80,80); //open mouth
    noFill();
    ellipse(575,175,20,20);
    noFill();
    ellipse(550,150,25,25);
    noFill();
    ellipse(500,90,150,100);
    fill(0);
    textSize(25);
    text('bla bla bla..',435,100);

  } else {
    fill(0);
    line(675,380,725,380); //closed mouth
  }

  //left eye:
  fill(255);
  ellipse(640,270,75,75);

  //right eye:
  fill(255);
  ellipse(760,270,75,75);

  //rotation pupils:

//centrum 700,300

  if (mouseX<700 && mouseY<300){ //øverst til venstre
    fill(0);
    ellipse(750,250,30,30);  //højre pupil
    fill(0);
    ellipse(630,250,30,30);

 } if (mouseX>700 && mouseY<300){ //øverst højre
    fill(0);
    ellipse(770,250,30,30); //højre pupil
    fill(0);
    ellipse(650,250,30,30);

} if(mouseX<700 && mouseY>300){ // nederst venstre
    fill(0);
    ellipse(750,290,30,30); //højre pupil
    fill(0);
    ellipse(630,290,30,30);

 } if(mouseX>700 && mouseY>300){ //nederst højre
    fill(0);
    ellipse(770,290,30,30); //højre pupil
    fill(0);
    ellipse(650,290,30,30);
 }

 fill(0);
 textSize(20);
 text("Tap to make emoji talk",620,500);

}
