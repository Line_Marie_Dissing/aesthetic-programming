# Readme miniX2

[Runme!](https://line_marie_dissing.gitlab.io/aesthetic-programming/miniX2/)

[Se my code](https://line_marie_dissing.gitlab.io/aesthetic-programming/ )

Screenshots of my sketch:

![](miniX2.png)


##### Describe your program and what you have used and learnt

My program is two emojis that both have the shape of an ellipse with eyes and a mouth made of different shapes inside of the ellipse. Both emojis have eyes and pupils made of ellipses as well and the pupils are bouncing around in the eyes on both emojis. In the first emoji the pupils are bouncing around by them self which I have made them do by using the syntax random () when I sat the parameter for x and y. In the second emoji the pupils are bouncing around by following the mouse which I have made them do by “dividing” the eyes in four and made an if () statement for each of the four parts of the eye. Emojis two also opens the mouth which is seen as an ellipse whit random colors inside when you click on the mouse. Besides of opening the mouth the emoji also gets a kind of speech bubble when you click the mouse. 


From my program I have learned to use new syntaxes as the if () and else () statements.  




##### How would you put your emoji into a wider social and cultural context that concerns a politics of representation, identity, race, colonialism, and so on? (Try to think through the assigned reading and your coding process, and then expand that to your own experience and thoughts - this is a difficult task, you may need to spend some time thinking about it). 

My idea was to make my emojis as neutral as possible and therefore I took as my starting point the “classic” emoji consisting of a yellow ellipse with a face inside of it. Still, some may think that the color of my emojis (yellow) can be seen as politically incorrect due to the new way to choose skin color on emojis as Femke Snelting describes in the video “Modifying the Universal”. _“Our point is that using emojis may be fun and expressive, but they also tend to oversimplify and universalize differences, thereby perpetuating normative ideologies within already “violent power structures,” such that only selected people, those with specific skin tones for instance, are represented while others are not.”_ (Soon p. 56).  

Due to this ability to change the skin color on emojis yellow is no longer necessarily seen as a neutral color for emojis which was my intention when I made my emojis yellow. Beside of that I tried to make my emojis very simple without a lot of details, so I didn’t end up whit emojis too human like.

I thought it was a difficult task to complete because of all the political and social aspects there is to emojis. All people are different and sees emojis in different ways. And due to that I may not have succeeded in making fun and non-offensive emojis, even though this was my goal for the task.  


#### References:

[Modifying the Universal—Femke Snelting](https://www.youtube.com/watch?v=ZP2bQ_4Q7DY)

[3.3: Else and Else if, AND and OR - p5.js Tutorial](https://www.youtube.com/watch?v=r2S7j54I68c&t=191s)
