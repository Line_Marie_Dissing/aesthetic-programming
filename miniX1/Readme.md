# Readme miniX1

[See my project!](https://line_marie_dissing.gitlab.io/aesthetic-programming/miniX1/)

[See my code](https://gitlab.com/Line_Marie_Dissing/aesthetic-programming/-/blob/ab64116ad3ab9cec6a578a6751b8be79eb2d0179/miniX1/sketch.js?fbclid=IwAR2XFk2qW7uTPiPaNM4QYstaeLomwkO1A5tKoF2q9fK--TLGzxOD_LDQGBE)

![](miniX1.png) 

### What have I produced?: 
I have created a canvas where the background has a random color within the color scale I have put in the code (random (255),150,250): giving blue and purple colors. Under _function draw _ I have created three rectangles within each other in different sizes and colors. To give the rectangles the same center, I have put in _rectMode(center)_ ind the _function setup_. The x and y location of the rectangels follows the mouse which results in that when you move the mouse, the rectangles will follow and draw ‘lines’ on the canvas. Finally, I have added a function that clears the screen: To clear the screen and set a new random color within the color scale, I have put in the function that when you click on the mouse, the screen clears and a new background color pups up the function of the three rectangles start over. Eventually I made the speed slower. 

### My first coding experience:
I didn’t know where to start, so I went on the ‘open processing’ website and search for ‘sun’ and found the creation linked as inspiration under references. Then I started by writhing down the exact code from the website to see, if I could get the code right by writing it down myself. Then I went on the p5.js website and looked at the references. After looking around in the references I started to modify the code to make it my own by changing the shape of the ellipses to rectangles and the background to static, so the rectangle not just follow the mouse but draw on the background.   

### How is the coding process different from, or similar to, reading and writing text?:
I think that coding and learning to code is like learning any other (new) langue. In order to understand and writhe a langue you need to know the gramma rule and the meaning of the words. I think the same goes for coding. You need to know the rules and meaning of the words in coding if you want to be able to read the coding of others or make a code yourself. Also, in a “normal” langue if you forget to put a word in a sentence, the sentence does not make sense similar to coding; if you forget to put in a letter or sign the code does not work. 

### What does code and programming mean to you, and how does the assigned reading help you to further reflect on these terms?: 
I don’t know have any experience in programming in advance and I therefore doesn’t really know what it means to me. In the first week of this course, I have learned a little about code and programming; I know it’s a tool to create something and it is both fun and frustrating at the same time. I think for sure that the assigned reading will help me to understand and reflect on these terms. 


### Reference
[Inspiration](https://openprocessing.org/sketch/1071229)
