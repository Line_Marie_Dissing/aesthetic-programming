//Thobber SolarSystem

var stars = [3,5,2,6,4]; //array for size of the stars

var sunTexture;
var earthTexture;
var saturTexture;
var jupiterTexture;
var mekurTexture;
var venusTexture;
var marsTexture;
var neptunTexture;
var uranusTexture;

function preload() {
  sunTexture = loadImage('sun.png');
  earthTexture = loadImage('EarthPNG.png');
  saturTexture = loadImage('satur.png');
  jupiterTexture = loadImage('jupiter.png');
  mekurTexture = loadImage('mekur.png');
  venusTexture = loadImage('venus.png');
  marsTexture = loadImage('mars.png');
  neptunTexture = loadImage('neptune.png');
  uranusTexture = loadImage('uranus.png');

}

function setup() {
  createCanvas(windowWidth,windowHeight);
  frameRate(5);

}

function draw(){
  background(50,150);
  let num=9;


  //let starX = random(1,windowWidth);
//  let starY = random(1, windowHeight);

  //stars
  for(var i = 0; i < 4; i++){
    let starX = random(1,windowWidth);
    let starY = random(1, windowHeight);
    noStroke();
    fill(255);
    ellipse(starX, starY, stars[i]);

  }
  // the stars I first made:
  /* ellipse(30,50,5,5);
  ellipse(100,55,5,5);
  ellipse(800,30,5,5);
  ellipse(150,600,5,5);
  ellipse(560,150,5,5);
  ellipse(990,250,5,5);
  ellipse(1000,500,5,5);
  ellipse(1030,490,5,5);
  ellipse(1060,480,5,5);
  ellipse(660,350,5,5); */

  //sun
  push();
  translate(width/2, height/2); //moves things to center
  let cir = 360/num*(frameCount%num);
  rotate(radians(cir));
  imageMode(CENTER);
  image(sunTexture,0,0,100,100);

  /*noStroke();
  fill(255,196,0);
  ellipse(0,0,100,103);*/

  // The elliptical orbits of the planets
  noFill();
	stroke(240, 240, 240, 80);
	strokeWeight(1);
	ellipse(0, 0, 165, 165); //mekur
	ellipse(0, 0, 280, 280); //vensus
	ellipse(0, 0, 390, 390); //Earth
  ellipse(0, 0, 465, 465); //mars
  ellipse(0, 0, 605, 605); //jupiter
  ellipse(0, 0, 750, 750); //satur
  ellipse(0, 0, 825, 825); //uranus
  ellipse(0, 0, 920, 920); //neptun

  pop();
  // push og pop gør at det kun er inde mellem dem der sker det der er kodet der

  //mekur
  push();
  translate(width/2, height/2);
  let cir1 = 360/18*(frameCount%18);
  rotate(radians(cir1));
  image(mekurTexture,75,0,15,15);
  /*
  noStroke();
  fill(128,115,102);
  ellipse(75,0,15,15);
  */
  pop();

  //venus
  push();
  translate(width/2, height/2);
  let cir2 = 360/27*(frameCount%27);
  rotate(radians(cir2));
  image(venusTexture,125,0,30,30);
  /*noStroke();
  fill(236,156,87);
  ellipse(125,0,30,30);*/
  pop();

  //Earth
  push();
  translate(width/2, height/2);
  let cir3 = 360/36*(frameCount%36);
  rotate(radians(cir3));
  //noStroke();
  //fill(88,188,230);
  //ellipse(175,0,30,30);
  image(earthTexture,175,0,35,35);
  pop();

  //mars
  push();
  translate(width/2, height/2);
  let cir4 = 360/45*(frameCount%45);
  rotate(radians(cir4));
  image(marsTexture,225,0,15,15);
  /*noStroke();
  fill(249,77,3);
  ellipse(225,0,10,10); */
  pop();

  //jupiter
  push();
  translate(width/2, height/2);
  let cir5 = 360/54*(frameCount%54);
  rotate(radians(cir5));
  image(jupiterTexture,275,0,60,60);
  /*
  noStroke();
  fill(204,153,102);
  ellipse(275,0,60,60);
  */
  pop();

  //satur
  push();
  translate(width/2, height/2);
  let cir6 = 360/63*(frameCount%63);
  rotate(radians(cir6));
  image(saturTexture,350,0,45,50);
  /*
  noStroke();
  fill(213,188,177);
  ellipse(350,0,45,45);
  noFill();
  stroke(215,188,177);
  strokeWeight(3);
  ellipse(355,0,15,70)
  */
  pop();

  //uranus
  push();
  translate(width/2, height/2);
  let cir7 = 360/72*(frameCount%72);
  rotate(radians(cir7));
  image(uranusTexture,400,0,25,25);
  /*noStroke();
  fill(24,232,239);
  ellipse(400,0,25,25); */
  pop();

  //neptun
  push();
  translate(width/2, height/2);
  let cir8 = 360/80*(frameCount%80);
  rotate(radians(cir8));
  image(neptunTexture,450,0,20,20);
  /* noStroke();
  fill(0,68,255);
  ellipse(450,0,20,20); */
  pop();

}


function windowResized(){
  resizeCanvas(windowWidth, windowHeight);

} //Gør at canvas følger med, når man ændre str på vindueskærmen
