# MiniX 3

[Link to my code](https://gitlab.com/Line_Marie_Dissing/aesthetic-programming/-/blob/a5124c0822fbbb8e0fb0bcd43cd18127759f676e/miniX3/sketch.js)

[See my project!](https://line_marie_dissing.gitlab.io/aesthetic-programming/miniX3)


**GIF of my program:**

![](miniX.gif)



_The purpose of this miniX was to reflect upon temporality in digital culture by designing a throbber icon and to experiment with various computational syntaxes and the effects of animation, and transformation._ 


First, I just wanted to make a “normal” throbber like the one on Netflix and YouTube eg by using Winnie’s program as the core foundation (Soon, Cox 2020, p. 77). But instead of circle of ellipses going around I wanted to make serval circles for ellipses outside on each other by adding 50 pixels every time I made a new throbber. Moreover, I wanted to give it roughly the same speed rather than those with larger circuits "jumping" around if they had only 9 ellipses like the inner one. Also, by doing that, It gave the ellipses some kind of “shadow-tails.” In order to do that, I changed the numbers of ellipses in the circle by adding 9 every time I made a new throbber outside another. 

When I have finished the way I first wanted to make my throbber I thought it looked a little like the Solar system and because of that, I decided to change my program till the solar system. To do that I started by creating a serval more ellipses, so it matched the number of planets in the solar system – each planet has is inside its own _push()_ and _pop()_ function. Then I added the elliptical orbits of the planets and changed the sizes and colors of the ellipses to make them look like the planets in the solar system. 
Afterwards I chose to replace the ellipses I had made with pictures of the planets to make it look more real. The original ellipses I made as the planets can still be seen in the code, but is commented out :) 
I chose to keep the “shadow-tails” because I think it’s very characteristic of a throbber.  
Finally I added stars to the space made out of white ellipses. First, I added the stars by making an ellipse for every one of them but then I tried to make them by using _Arrays_ and _for loops_. When I made the stars by doing that, I could not make serval stars on the canvas at the time but instead the stars are moving around in the canvas which is the ones that can be seen in my program. Like the original planets I have also commented out the original stars that I made in my code.  

So, when I started my program, I wanted to express a throbber like the one on Netflix but just with more throbbers outside each other and in different colors but instead I ended up with a throbber that looked like the solar system. 

I think that many of the throbbers I have encounted in digital culture all clearly shows that something is loading, and you have to wait. Yet, most of them does not express what is loading or how much time is left of the loading. 
The same I think goes for my own throbber; unless it is used to something relating to the space it is not related to the program loading. However, when I decided to make the solar system my goal was not to make clear connections between my throbber and what is loading. Instead, I wanted to make connect it to the experience of time: 

> ”The experience of time is an essential element of any form of experience or cognition. Emotions depend to a large extent on expectations, or the potential coming of a future event. Any observ- ing or experience of difference, of presence related to an earlier or later absence, is linked with an experience of time. […] Also, the experience of time is a conglomerate of different experiences: time as a common moment, the duration of a certain time, time as cyclic events, historical time, and so on”. (Lammerant 2018, p 89)

As mentioned in the quote the experience of time is different to people, but even though people experience time in different ways the solar system always have to same time/ speed. So, by making my throbber look like a solar system I thought about that no matter how long you have to wait for something loading and your experience of that the solar system will always have to same time/ speed. 


I have chosen to focus on the syntaxes _push()_ and _pop()_ to create each planet and to make them independent in their movement location on the canvas. For time-related syntaxes I have used the syntax frameRate(5); to lower the speed from 60 refresh pr second to 5 refresh pr second. I have also used a for loop to make my starts appear by setting up a conditional structure: 

> ”A “for-loop” allows code to be executed repeatedly, and so provides an efficient way to draw the line one hundred times by setting up a conditional structure, counting the number of lines that have been drawn and counting the maximum number of lines.” (Soon and Cox 2020, p. 91).

When I used the for loop for my starts my goal was, as mentioned, to make serval stars appear at the same time without creating each star separately which however did not succeed for me. Still, I think the result of the starts appearing random on the canvas is nice. 







#### References

Hans Lammerant, “How humans and machines negotiate experience of time,” in The Techno-Galactic Guide to Software Observation, 88-98, (2018)

Soon Winnie & Cox, Geoff, "Infinite Loops", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 71-96

**Daniel Shiffman:**

[For loops](https://www.youtube.com/watch?v=cnRD9o6odjk)

[Arrays and loops](https://www.youtube.com/watch?v=RXWO3mFuW-I)


##### Inspiration:
https://openprocessing.org/sketch/1008779










