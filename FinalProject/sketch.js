// video capture-part: https://stackoverflow.com/questions/60982266/p5-js-extracting-images-from-createcapture-feed
// setTimeout: https://www.youtube.com/watch?v=nGfTjA8qNDA

//create global variables:
let video;
let pics = []
let button;
let idx = 0
let throbber;
let fullName;
let emailAdress;
let country;
let password;
let birthMonth;
let birthDay;
let birthYear;
let gender;
let agreeTerms;
let createAccount;
let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
let throbberActivated = false
let factsData;

//Preload the json file
function preload(){
  factsData = loadJSON("facts.json")
}

function setup() {
  createCanvas(800,600);

  frameRate (8); //set the  framerate
  throbber = new Throbber(); //calls the class "Throbber"

  startPage(); //call startpage function

  video = createCapture(VIDEO); //create a function where our video is
  video.hide(); //hide the videoCapture for the user
  video.size(160,120); //size of the videoCapture

}



function draw() {

// create conditions for when the throbber the activated:
  if(throbberActivated){
    showThrobber();
  }
  }


// Creates the "create account fill in side" as a function:
function startPage(){
  noStroke();
  fill(230);

//creates the grey rectangles to center the "fill in formular":
  rect(0, 0, 200, 600);
  rect(600, 0, 200, 600);
  rect(200,0,400,15);
  rect(200,height-15,400,15);

//Create text at the top of the "create account fill in side"
  fill(0);
  textSize(30);
  textAlign(LEFT);
  textFont("Times New Roman");
  text("Account creation", 270, 70);

  fill("GREY");
  textSize(15);
  textAlign(LEFT);
  textFont("Times New Roman");
  text("Enter the information below to setup account", 270,95);

//create input for full name:
  fullName = createInput().attribute('placeholder','Full Name *'); //use placeholder to make the text "invisible"
  fullName.position(270,140); //position of the input field
  //text above the input field:
  textSize(15);
  textAlign(LEFT);
  fill("RED");
  text("Full name *", 270, 135);

//create input for emailadress:
  emailAdress = createInput().attribute('placeholder','E-mail adress *'); //use placeholder to make the text "invisible"
  emailAdress.position(270,205); //position of the input field
  //text above the input field:
  textSize(15);
  textAlign(LEFT);
  fill("RED");
  text("E-mail adress *", 270, 200);

//create input for country:
  country = createInput().attribute('placeholder','Country of origin *'); //use placeholder to make the text "invisible"
  country.position(270,270); //position of the input field
  //text above the input field:
  textSize(15);
  textAlign(LEFT);
  fill("RED");
  text("Country of origin *", 270, 265);

//create input for password:
  password = createInput().attribute('placeholder','Password *'); //use placeholder to make the text "invisible"
  password.position(270,335); //position of the input field
  //text above the input field:
  textSize(15);
  textAlign(LEFT);
  fill("RED");
  text("Password *", 270, 330);

//Selecter to chose the month of birth
  birthMonth = createSelect();
  birthMonth.position(310,430); //position of the selector
  for(let i = 0; i <= 11; i++){ //the options to chose between
    birthMonth.option(months[i]);
  }

//Selecter to chose the day of birth
  birthDay = createSelect();
  birthDay.position(270,430); //position of the selector
  for(let i = 0; i < 30; i++){
    birthDay.option(i+1); //the options to chose between
  }

//Selecter to chose the year of birth
  birthYear = createSelect();
  birthYear.position(400,430); //position of the selector
  for(let i = 1940; i < 2021; i++){
    birthYear.option(i); //the options to chose between
  }

//Selecter to chose the gender
  gender = createSelect();
  gender.position(270, 385); //position of the selector
  gender.option("Chose gender");
  //the options to chose between:
  gender.option("Other");
  gender.option("Female");
  gender.option("Male");

//The create account button
//tool to change the button: https://www.w3schools.com/css/css3_buttons.asp
  createAccount = createButton("Create Account"); //create the button
  createAccount.position(270, 515); //position of the button
  createAccount.style("font-size", "14px"); //size of the button
  createAccount.style("text-align", "center"); //to put the text in the middle of the button
  createAccount.style("text-decoration", "Times New Roman");
  createAccount.style("background", "Green"); //the color of the button
  createAccount.style("color",  "White"); //the text of the button
  createAccount.style("border", "none");
  createAccount.style("border-radius", "5px"); //the border of the button
  createAccount.size(180,25); //size of the button
  createAccount.mousePressed(switchBackground); //Go to the function "switchBackground" when mouse is pressed


//Create the checkbox to "terms and conditions"
  agreeTerms = createCheckbox("Agree to terms and conditions * "); //checkbox
  agreeTerms.position(270,470); //position of the checkbox
  //agreeTerms.changed(switchBackground); //checks if something is changes about the function checked();
}

function switchBackground() {
  //Checks if there is at least 4 letters filled in
  if(fullName.value().length < 4){
    fullName.style("background",  "PINK");
    console.log("write your name");
    return;
  }if(emailAdress.value().search("@") == -1){
    //checks if there is a "@" filled in
    emailAdress.style("background",  "PINK");
    console.log("Your email must contain @");
    return;
  }if(country.value().length < 3){
    //Checks  if  there  is at least 3 letters filled  in
    country.style("background",  "PINK");
    console.log("not a country");
    return;
  }if(password.value().length < 7){
    //Checks  if  there  is at least 7 letters filled  in
    password.style("background",  "PINK");
      console.log("Too short");
      return;
  }if (agreeTerms.checked()) {
    //checks if terms and conditions are marked
      console.log('Checked');
    } else {
      console.log('Not checked');
      return;
    }
console.log("everything is good");

//To hide all the DOM elements when the throbber appears
fullName.hide();
emailAdress.hide();
country.hide();
password.hide();
agreeTerms.hide();
createAccount.hide();
birthDay.hide();
birthYear.hide();
birthMonth.hide();
gender.hide();

//To hide all the text and grey rectangles when the throbber appears
noStroke();
fill(255);
rect(260, 30, 290,150);
rect(280, 135, 120,30);
rect(270, 170, 100, 30);
rect(270, 240, 120, 30);
rect(270, 310, 120, 30);
rect(0, 0, 200, 600);
rect(600, 0, 200, 600);
rect(200,0,400,15);
rect(200,height-15,400,15);

//To activate the  throbber
throbberActivated  = true;

//captures the picture
let img = video.get(0,0,160,120)
pics.push(img);

}


// show throbber
function showThrobber(){
    throbber.move(); // set movement to the throbber from  the class
    setTimeout(lastPage, random(2000, 5000)); //set the throbber to change to the final page after 2-5 sec.
}

//the final page that appears after the throbber:
function lastPage(){
  throbberActivated = false; //disable throbber
  background(255);

// make text from the json file appear on the canvas:

//Chose random text from the json file
  let fact1 = random(factsData.dataFacts);
  let fact2 = random(factsData.dataFacts);
  let fact3 = random(factsData.dataFacts);
  let fact4 = random(factsData.dataFacts);
  let fact5 = random(factsData.dataFacts);
  let fact6 = random(factsData.dataFacts);

// placement of the text from the json file
  fill(0);
  textAlign(CENTER);
  textSize(random(10,15));
  textFont("Times New Roman");
  text(fact1, 140, 50);
  text(fact2, 300,185);
  text(fact3, 650, 400);
  text(fact4, 500, 80);
  text(fact5, 140, 450);
  text(fact6, 350, 550);



//to make  the pictures appear on the canvas:
	for(var i = 0; i < pics.length; i++) {
    imageMode(CENTER);
    //positions of the pictures:
     image(pics[i], 200, 300);
		 image(pics[i], 400, 300);
		 image(pics[i], 600, 300);
}
}
