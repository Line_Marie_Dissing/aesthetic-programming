
//Soon and Cox 2020, p 77
class Throbber{
  constructor(){
    this.size = 22; //size of the ellipses
    this.num = 9 //numbers of ellipses
    this.cir = 360/this.num*(frameCount%this.num); //movement of  the ellipses
  }

move(){
  fill(0);
  textAlign(CENTER)
  textFont("Times New Roman");
  textSize(20);
  text("Please stand by...", width/2, 450); //The text below the throbber
  push();
  translate(width/2, height/2); //to move the throbber in the middle of the canvas
  noStroke();
  fill(255, 50);
  ellipse(0,0,110,110);
  console.log("hej");
  this.cir = 360/this.num*(frameCount%this.num);
  rotate(radians(this.cir)); //to make the trobber rotate
  noStroke();
  fill(10,200);
  //the x parameter is the ellipse's distance from the center
  ellipse(35, 0,this.size, this.size);
  pop();
}

}
