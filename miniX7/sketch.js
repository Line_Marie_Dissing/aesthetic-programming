//Det her er én måde at lave OOP på - men ikke den eneste måde!
//Det kan være en ide at kigge på messy-notes 07 til bedre forståelse

let paddleSize = {
  w:70,
  h:15
};

let sunimg;
let flowerimg;
let min_sunshine = 4; //minimum numbers os sunshine on the screen
let sunshine  = []; //et array --> lille "t", fordi det er objektet
let paddlePosX;
let mini_width;
let score =0, lose = 0;
let flowerX =150;
let flowerY = 200;



function preload(){
  sunimg = loadImage('sun.png');
  flowerimg =  loadImage('flower.png');
}


function setup(){
  createCanvas(windowWidth,windowHeight);
  paddlePosX = width/2;
  mini_width = width;


}


function draw(){
  background("#87CEFA");
  image(flowerimg, windowWidth-100,windowHeight-50, flowerX, flowerY);
  //makeFlowerGrow();
  displayScore();
  checkSunshineNum();
  showSunshine();
  noStroke();
  fill("#90EE90");
  rect(paddlePosX, height-15, paddleSize.w, paddleSize.h);
  checkCaught(); //bruges til at tjekke om det er fanget og dermed bliver slettet
  checkResult();
}

function checkSunshineNum(){
  if(sunshine.length < min_sunshine){
    sunshine.push(new Sunshine());
  }
}

function showSunshine(){
  for(let i=0; i < sunshine.length; i++){
    sunshine[i].move();
    sunshine[i].show();
  }
}


function checkCaught(){
  for (let i = 0; i < sunshine.length; i++) {
  let d = int(
    dist(paddlePosX + paddleSize.w/2, height-paddleSize.h/2,
      sunshine[i].pos.x, sunshine[i].pos.y)
    );
  if (d < paddleSize.w/3) { //close enough as if catching the sunshine
    score++;
    sunshine.splice(i,1);
  }else if (sunshine[i].pos.y > height) { //paddle missed the sunshine
    lose++;
  sunshine.splice(i,1);

    }
  }
}

//At this part I wanted to make the flower change size based on the score
//but I could not make it work.
/*
function makeFlowerGrow(){
  //for (let i = 0; )
  let flowerX =150;
  let flowerY = 200;
  image(flowerimg, windowWidth-100,windowHeight-50, flowerX, flowerY);

  if(score = score++){
    flowerX + 50;
    flowerY + 50;
  }

  if(lose = lose++){
    flowerX - 50;
    flowerY - 50;
  }

} */



function displayScore() {
    fill("#FFA500");
    textSize(17);
    text('You have caught '+ score + "sunsine(s)", 10, height/1.4);
    text('You have lost ' + lose + " sunsine(s)", 10, height/1.4+20);
    fill("#FF8C00");
    text('PRESS the ARROW LEFT & RIGTH key to catch Sunshine',
    10, height/1.4+40);
}


function checkResult() {
  if (lose > score && lose > 4) {
    fill("#FFD700");
    textSize(30);
    text("You have lost to much sunshine...GAME OVER", width/3, height/1.4);
    noLoop();
  }
}



function keyPressed() {
  if (keyCode === LEFT_ARROW) {
    paddlePosX-=45;
  } else if (keyCode === RIGHT_ARROW) {
    paddlePosX+=45;
  }
  //reset if the paddle moves out of range
  if (paddlePosX > mini_width) {
    paddlePosX = mini_width;
  } else if (paddlePosX < 0 - paddleSize.w/2) {
    paddlePosX = 0;
  }
}
