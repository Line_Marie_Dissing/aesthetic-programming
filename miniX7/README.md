## Readme 7: games with objects

[Click here to catch sunshine!](https://line_marie_dissing.gitlab.io/aesthetic-programming/miniX7)


![](miniX7.GIF)



[Se to have a look at my code](https://gitlab.com/Line_Marie_Dissing/aesthetic-programming/-/blob/master/miniX7/sketch.js)

[See my class](https://gitlab.com/Line_Marie_Dissing/aesthetic-programming/-/blob/master/miniX7/classsunshine.js)


In my miniX for this week I have made a game where it's about catching sunshine whit a paddle in the bottom of the screen – you move the paddle by using the left and right arrow on the keyboard. You actually can’t win the game, but you lose the game when the sum of lost suns is 4 more than the suns you have caught. I have also put a flower in the right side of the canvas which ended up just being for decoration on my canvas. My purpose with the flower was to make it change size based on the sunshine’s you catch and lose. However, I could not make it work – see my code :)
When I made the game, I took starting point in tofu game that we went through in class but changed the game from horizontally to vertically and changed the tofu(s) to sunshine and the pacman to a paddle. 

The objects in the program are the suns that moves from the top on the screen to the bottom in different sizes. I have made the suns by creating a class called “Sunshine”. I have made the sun by using an image and the attributes is therefore yellow as the color and a sun with sunrays as the shape. The object have two methods: `move( );` and `show( );`

- `Move( );` makes the suns move from the top of the screen to the bottom and out of the screen. Besides of that it makes the suns moves with different speed. 
- `Show( );` creates the suns in different sizes and makes them appear random on the x-axis and at the point 0 on the y-axis. 


When I made this miniZX I wanted to create something that was not too difficult and instead focus on understanding the concept of OOP. This was also the reason why I chose to take starting point in the tofu-game. I thought that by changing the code and make it my own I would get to know the function on all the syntaxes. For a start I changed the movement of the tofu form horizontally to vertically and then I tried to figure out which object I wanted instead of the tofus. Then I ended up making suns – which I really just did because I ache for sun and summer. 



Looking at the assigned reading I actually think it is a little bit difficult to understand the meaning of abstractions. Soon and Cox describes object abstractions: 

> “Object abstraction in computing is about representation. Certain attributes and relations are abstracted from the real world, whilst simultaneously leaving details and contexts out.” (Soon and Cox 2020, p. 146).

From this I think that the use of a sun as my objects does that, I have avoided any too complex abstractions but still it should be mentioned that the created objects are usually very subjective the programmer has selected things for attention and other attributes are ignored.  







#### References:

- Soon Winnie & Cox, Geoff, "Object abstraction", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 143-164

- [Color names](https://www.w3schools.com/colors/colors_names.asp)
