//create/name your class - navnet sætter man til at starte med stort begyndelsesbogstav:
class Sunshine{
  constructor(){
    this.speed = floor(random(1,4));
    this.size = floor(random(35, 60));
    this.pos = new createVector(random(windowWidth), 0);  //--> pos.x and pos.y
    //--> hvis man vil have dem forskelligt på y-aksen skal 200 ændres til random(windowsHeight)
  }

//Følgende to (move and show) er behaviour af Tofu
  move(){
    this.pos.y += this.speed; //this.pos.x = this.pos.x - this.speed
  }
  //du bliver nødt til at fortælle computeren, hvad den skal vise
  show(){
    imageMode(CENTER);
    image(sunimg, this.pos.x, this.pos.y, this.size, this.size);
  }

  }

//this. bruges for at beskrive, at det man sætter efter tilhøre den class det står i.
