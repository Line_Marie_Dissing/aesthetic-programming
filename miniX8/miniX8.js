let imageText;
let textWomen;
let factWomen;


function preload(){
  imageText = loadImage("textme.jpeg");
  textWomen = loadJSON("text.json");
  factWomen = loadJSON("facts.json");

}

function setup (){
  createCanvas(1200, 800);
  background(255);
  imageMode(CENTER);
  image(imageText, 600, 400, 400, 800);
  //image(imageText, windowWidth/2, windowHeight/2, windowWidth/2.5, windowHeight);
  console.log(textWomen);
  console.log(factWomen);
  frameRate(1);
}

function draw(){
  /*textSize(15);
  fill(10,20,40,90);
  //textAlign(LEFT);
  textFont("American Typewriter");
  text(textWomen[2].myStatement, 10, random(windowHeight)); */

  for (let i = 0; i <5; i++){
  textSize(random(5,15));
  fill(10,20,40,90);
  textAlign(LEFT);
  textFont("American Typewriter");
  text(textWomen[i].myStatement, 10, random(windowHeight));
}

for (let j = 0; j < 6; j++){
  textSize(random(5,15));
  fill(10,20,40,90);
  textAlign(RIGHT);
  textFont("American Typewriter");
  text(factWomen[j].assaultFacts, 1190, random(windowHeight));

}

}
