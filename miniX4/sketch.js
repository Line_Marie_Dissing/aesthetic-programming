// The like selector

let output = ["liked", "disliked"];
let button;
let numberOfLikes = 0;
let numberOfDislikes = 0;

let beach;

function preload (){
  beach = loadImage('beach.png');

}



function setup(){
  createCanvas(640,480);
  background(255);

  frameRate(5);

  button = createButton("Like");
  button.style("background-color", "#A9A9A9");
  button.size(75,25);
  button.position(50, 350);
}

function newOutcome() {
  updateLikes();
  button.html(random(output));

}


function change(){
  button.style("background-color", "#696969");
  }

  function revertStyle() {
    button.style("background-color", "#A9A9A9");
  }


function draw(){
  background(255);

  image(beach, 50, 90, 225, 250);

  button.mousePressed(newOutcome);
  button.mouseOver(change);
  button.mouseOut(revertStyle);

  text("likes:", 50, 400);
  text(numberOfLikes, 120, 400);
  text("dislikes:", 50, 420);
  text(numberOfDislikes, 120, 420);


if (numberOfDislikes > 99){
  clear(beach);
  text ("Your image has received too many dislikes \n It has therefore been deleted", 50, 200)



}

}

function updateLikes (){

  if (button.html() == "liked"){
    numberOfLikes ++;
    print(numberOfLikes);

  } if (button.html() == "disliked"){
    numberOfDislikes ++;
  }

}






/*
function windowResized(){
  resizeCanvas(windowWidth, windowHeight);
} */


/*måske noget med at der er en knap man skal trykke på, men man ved ikke,
om man liker eller disliker, når man trykker på knappen. Det får man først
at vide efter man har trykket på knappen --> og man kan ikke ændre valg*/
// kan gøres ved brug af random og arrays

//CSS textsize button
