# MiniX 4

_Please note that it is necessary to press the "like button" until there are 100 dislikes to see everything that happens in my program_

[Runme!](https://line_marie_dissing.gitlab.io/aesthetic-programming/miniX4/)

[See my code](https://gitlab.com/Line_Marie_Dissing/aesthetic-programming/-/blob/master/miniX4/sketch.js)

![](miniX4.png)

## “The like selector”
I have produced a random like-button called “The like selector” and is meant for social medias like Instagram.  When you see the like button, it says “like” like any normal like button, but when you click on the button you will either get the outcome “liked” or the outcome “disliked”. So even though your purpose was to like the picture, you might actually end up disliking it. The likes and dislikes are sat to a random selector, so there is no patten in the numbers of likes and dislikes – it is random how many likes and dislikes you get. Furthermore, I have put in a limit of dislikes on the button, so when a picture gets 100 dislikes the picture will be deleted, and the person who have shared the photo will get the following message: "Your photo has received too many dislikes. It has therefore been deleted"


My purpose with my program was to focus on how the "like" button is not as innocent as one invariably thinks: 

> It is fairly clear how the clicks serve the interests of platform owners foremost, and, as if to prove the point, Facebook, and Instagram have tested the idea of hiding the metrics on posts in order to shift attention to what they prefer to call “connecting people” 23 — as if to prove their interests to be altruistic. (Soon and Cox 2020, p. 113)

Beside of the aspect of Date Capture, I also wanted to focus on the feelings and emotions the like-button brings with it – the pursuit of getting many likes in the hopes that the more likes you get the happier you will be and a lot of likes my give you confidence. Conversely, few likes can negatively affect your mood. As I see it, likes in that way is some kind of artificial self-confidence that makes us dependent on the feedback of others. 
My vision for my like button was to make a like-button, where it is not in the same way as the “normal” like button about getting many likes: It is random whether you like or dislike, when you press the button and because of that too many "likes" may ultimately cause your photo to be deleted. I have sat the numbers of dislikes before the photo is deleted to 100, but basically it could be any number. 
In my program I have used the syntaxes Arrays, if statements, the random syntax and the DOM element createButton and other syntaxes linked to the button function.  




#### References:

Soon Winnie & Cox, Geoff, "Infinite Loops", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 111-115

[W3schools, CSS button](https://www.w3schools.com/css/css3_buttons.asp)

[Unsplash, free photo download](https://unsplash.com)

**Daniel Shiffmann:**

[Manipulating DOM Elements with html() and position()](https://www.youtube.com/watch?v=YfaJ20vXcK8&list=PLRqwX-V7Uu6bI1SlcCRfLH79HZrFAtBvX&index=3)

[Handling DOM Events with Callbacks](https://www.youtube.com/watch?v=NcCEzzd9BGE&list=PLRqwX-V7Uu6bI1SlcCRfLH79HZrFAtBvX&index=4)

[Other Events and Inputs](https://www.youtube.com/watch?v=HsDVz2_Qgow&list=PLRqwX-V7Uu6bI1SlcCRfLH79HZrFAtBvX&index=6)

[Boolean Variables](https://www.youtube.com/watch?v=Rk-_syQluvc&t=2s)
