## Individual part – flowchart of previous miniX


**Flowchart of miniX7 (OOP)**
![](flowchartminiX7.png)
I have chosen to make a flowchart of my miniX7 (OOP) because I think it was the most technically complex one. The reason for that is that I first of all made to files for this miniX; the sketch and a file where I made my class for the program. Furthermore, this miniX is the one where I have created most of my own functions in my program. Therefore, I thought I would be nice to make a flowchart of this miniX to get a better understanding of how the different functions are related to each other and therefore get a better understanding of how my program is executed. 

_Below I have inserted a gif of my program from my miniX7 as well as a link to it so you can also see my sketches for the program._

![](miniX7.GIF)

[Link to my miniX7](https://gitlab.com/Line_Marie_Dissing/aesthetic-programming/-/tree/master/miniX7)


## Readme
**What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?**

Creating a flowchart is a method to elaborate your program and clarify the practical and conceptual aspects of the algorithm within.

> “Flowcharts, “Flow of the chart →  chart of the flow”, have been considered a fundamental explanatory tool since the early days of computer programming. One of their common uses is to illustrate computational operations and data processing for programming by “converting the numerical method into a series of steps.”” (Soon & Cox 2020 p. 214)

The difficulties involved within creating a flowchart and keeping things simple at the commutations level whilst maintaining the complexity is to translate everything from a coding language to the spoken language. It is almost like translating a German text in high school for the first time _(written in the group)_.

Also, even though we have tried to make a simple communication in our flowcharts, when making them we still discussed the technical part and how to make it so we would not make flowcharts of programs that we can’t make… When discussing how to make the flowcharts we several times ended up with more than one solution for a step which fit very well with a quote from Soon and Cox:
> “The diagram is an “image of thought,” in which thinking does not consist of problem-solving but — on the contrary — problem-posing.” (Soon and Cox 2020, p. 221)  



**What are the technical challenges facing the two ideas and how are you going to address these?**

Making the flowcharts and generating different ideas for this week's MiniX, were very different from just making the program to begin with. During the making of each flowchart, we were talking about how we were going to make each program, and which syntaxes we were going to use. We weren't quite sure of how to actually make each program but were looking around trying to figure out if the programs were even possible for us to make. We found some syntaxes, and some games that had some similarities to what we wanted, and from there we made the flowcharts knowing that the programs are possible to make, even though we are not sure yet, how to technically make them _(written in the group)_. 

Furthermore, are we not sure if it is possible to make our program exactly like we have written in the flowchart because even though we have found some syntaxes that should make it possible to make the overall idea we might have the change it at little when we start making the program to make it work. Especially because some of the syntaxes is syntaxes we have not used before e.g., the `createCapture();` we have found and want to use in the date capture program which is the one we like the most.   


**In which ways are the individual and the group flowcharts you produced useful?**

The flowcharts can be useful as a tool to communicate how you envision your idea. When making the group flowchart we also found that the flowcharts helped make clear what technical knowhow we need(ed) to create the programs we envision. While making clear what difficulties we would have technically, the flowchart also made clear how our ideas maybe weren’t as complicated as we first imagined. The flowcharts allowed us to quickly get an overview of our ideas and see if the ideas were possible for us _(written in the group)_.

> “If tasks need to be sub-divided among a group, for instance, flowcharts can be used to identify how a smaller task can be linked to others without losing site of the bigger picture.” (Soon and Cox 2020, p. 215)

I think the group flowchart is very useful to come to a common understanding of how we want our final project to end out. When we have a common understanding of how the program is going to end up it will be easier to individual work on different parts of the code and at the same time, we always know what the next step in coding-process is. 
Soon and Cox also points out the advantage of using flowcharts:

> “Similarly, in our teaching, we have used flowcharts as a means of deconstructing writing as well as to break down an argument in an essay structure, as a way to formulate new ideas and structure. Diagrams are good tools, or rather “machines,” that help us think through different procedures and processes, and this approach has evidently informed our use of flowcharts to introduce each chapter of this book.“ (Soon and Cox 2020, p. 215)  

The individual flowchart is useful to go back and look at the miniX we thought was the most technically complex one and get a better understanding of every syntax used in the program. Sometimes we might get inspired by looking at others programs and uses some of their code to make our own. Although we may already understand the program, we have coded I think the flowchart will give us an even more in-depth understanding of our program.



## Group part – brainstorm and flowchart for the final project 
For the final project we want to eighter make a program with the theme of OOP where we want to make a game or with the theme of data capture where we want to use the webcam.  Below are two flowcharts – one for each of them.

![](flowchartdatacapture.png)

![](flowchartOOP.png)

## References

Soon Winnie & Cox, Geoff, "Algorithmic procedures", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 211-226

