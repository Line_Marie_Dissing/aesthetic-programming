//10 PRINT

let x = 10;
let y = 10;
let spacing = 15;

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(255);

  frameRate(20);
}

function draw() {
  stroke(random(255), 55, 150);
  if (random(2) < 0.8) {
    line(x, y, x+spacing, y+spacing);
  } else {
    line(x, y-spacing, x-spacing, y);
  }
  x+=10;
  if (x > width) {
    x = 5;
    y += spacing;
  }
}
