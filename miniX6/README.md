## MiniX 6

_Please note that it is necessary to press the "like button" until there are 100 dislikes to see everything that happens in my program._

[Run my program!](https://line_marie_dissing.gitlab.io/aesthetic-programming/miniX6)

[Se my code](https://gitlab.com/Line_Marie_Dissing/aesthetic-programming/-/blob/bb9c950bf289574cc744ba2551b09bdef2d698b0/miniX6/sketch.js)

![](miniX4and6.png)

When I should decide which miniX I wanted to rework, I looked back on the feedback I have received. The reason I chose which miniX I wanted to rework based on my feedback is that I have tried to make all my miniX based on a critical mindset. Based on the feedback I chose to rework my miniX4 from the week focused on data capture. Back when I made the readme for this miniX I wrote the following description about my program: 

_”I have produced a random like-button called “The like selector” and is meant for social medias like Instagram. When you see the like button, it says “like” like any normal like button, but when you click on the button you will either get the outcome “liked” or the outcome “disliked”. So even though your purpose was to like the picture, you might actually end up disliking it. The likes and dislikes are sat to a random selector, so there is no patten in the numbers of likes and dislikes – it is random how many likes and dislikes you get. Furthermore, I have put in a limit of dislikes on the button, so when a picture gets 100 dislikes the picture will be deleted, and the person who have shared the photo will get the following message: "Your photo has received too many dislikes. It has therefore been deleted""_

The reason that I chose this from looking back at the feedback is that in the feedback I were asked the questions: “Why is it essential to use a random function in a singular button, instead of creating two seperate buttons (with the like and dislike)? Is there a conceptual meaning behind this choice?” 

When I made the like button, I wanted to focus on the feelings and emotions the like-button brings with it. Due to that my vision for my like button was to make a like-button, where it is not in the same way as the “normal” like button about getting many likes: It is random whether you like or dislike, when you press the button and because of that too many "likes" may ultimately cause your photo to be deleted – so both the likes and dislikes you get are in reality likes. So, to answer the questions in the previously feedback it was essential to use a random function in a singular button instead of two buttons: a like-button and a dislike-button. The conceptual meaning behind my choice was not, that people can dislike your photos but that you are not necessarily worth more the more likes you get. To link that with the theme for this week and critical making, I made my like button with the aim to highlighting some of the issues that come with likes on social media. To make it even more linked with critical making, my goal was not to make a solution to the problems I believe is connected to like buttons, but to create debate about it.

So, when I started to look back at this miniX I actually already think it fit very well to the concept of critical making:

> “The concept of critical making has many predecessors, all of which start with the assumption that built technological artifacts embody cultural values, and that technological development can be combined with cultural reflectivity to build provocative objects that encourage a re-evaluation of the role of technology in culture.” (Ratto and Hertz 2019, p. 19)

> "Ultimately, critical making is about turning that relationship between technology and society from a “matter of fact” into a “matter of concern” (Ratto & Hertz 2019, page 20)

Therefore, I did not want to change the concept of my miniX when I reworked it and I have because of that only made a change in the text that appear when the photo get 100 ”dislikes”. In addition to the text that originally appeared I have added: “But remember that you are not worth less because of a picture. You are still loved <3”. I added that to text to put even more focus to on the artificial self-confidence that likes can give us and to put focus on that some of the thoughts one might get if one does not get "enough" likes 


I think aesthetic programming in my program is demonstrated in the whole aspect of criticizing likes on social media, which is the focus of my program. I wanted to focus on and question some of the aspects of social media that affect us today. I think aesthetic programming can contribute this and the whole digital culture nowadays. 

> ”Aesthetic programming in this sense is considered as a practice to build things, and make worlds, but also produce immanent critique drawing upon computer science, art, and cultural theory. (Cox and Soon 2020, p. 15)” 

Lastly, I think programming as a practice or a method for design can be used to understand the way people act in the digital world and point out some of the problems the digital culture brings with it. When pointing out problems in the digital culture by using programming as a method for design you can point out the problems on the same platform as the problems takes place. 

##### References:
- Soon Winnie & Cox, Geoff, "Preface", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 12-24

- Ratto, Matt & Hertz, Garnet, “Critical Making and Interdisciplinary Learning: Making as a Bridge between Art, Science, Engineering and Social Interventions” In Bogers, Loes, and Letizia Chiappini, eds. The Critical Makers Reader: (Un)Learning Technology. the Institute of Network Cultures, Amsterdam, 2019, pp. 17-28

