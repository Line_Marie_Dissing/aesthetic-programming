## MiniX5

[Run my program](https://line_marie_dissing.gitlab.io/aesthetic-programming/miniX5)

[Link to see the code](https://gitlab.com/Line_Marie_Dissing/aesthetic-programming/-/blob/master/miniX5/sketch.js)

##### GIF of my program
![](miniX5.GIF)


When I sat down with a blank sheet of paper and thought of rule, I thought it would be easiest to make some rules about changing color and shape. Also, I thought this miniX was difficult compared to some of the previous, which was due to the fact that I wanted to make some simple rules for my program. Therefore, I have made the following two rules for my program: 

**Rule 1:** Every time the color is blue, then it must turn either pink or purple. If the color is pink or purple, it must turn blue. I did that by using a `if-statement` and the `random() `syntax. 

**Rule 2:** Every time the color changes the shape change depending on the color


Besides of the two rule I have made my program inspired by the 10 PRINT and therefore the shapes in my program goes from the left side of the page to the right side and every time the shapes hit the right side, they skip a line down and start from the left side again

In addition to the movement in my program I have used a `for loop` to make a grid in the canvas. I tried to make my grid look a little like the note paper with the small squares on it which was the kind of paper I used when I made the rules for my program. I thought I would give my program some kind of dimension instead of just a blank canvas. 

I have used the random syntax (rule 1) in a way a bit reminiscent of the one used in 10 PRINT:

> “The RND command in 10 PRINT selects one of two graphical characters—a kind of textual composition that recalls the last of these meanings of random. 10 PRINT’s random is a flip or flop, a symbol like a slash forward or backward (but fortunately less fearsome than the horseman’s random slash).” (Montfort et al., 2012, p. 121)

Beside the two slashes forward and backward I have added a third shape to the random syntax. Furthermore, I have put in the rule, that the blue forward slash will always occur every other time while it is random if it is the purple backward slash or the pink rectangle that appear in continuation of the blue forward slash. 


I think that even though you use the random syntax it will never be completely random, because when you use the random syntax you also set some rules and the program will therefore be generated based on these rules. Due to that I does not think that something is completely random when it is computer based. To link it with the assigned reading Montfort et al. writes about pseudorandomness:

> “(...)the significance of “pseudorandomness”—the production of random-like values that may appear at first to be some sad, failed attempt at randomness, but which is useful and even desirable in many cases (Montfort et al. p. 120)

Finally, in the tutorial session we were asked to think about who the artist is: Whether it ourselves ore it is the computer. I thought about that making my program and come up with the conclusion, that I think the computer is the artist in my program. The reason for that is, that I used the random function – so even though I made up the rules for the program it is the random function that has the final say in the outcome although it follows my rules. 


### References:

Nick Montfort et al., “Randomness”, in Montfort et al, 10 PRINT CHR$(205.5+RND(1)); : GOTO 10, Cambridge, MA: MIT Press, 2012, pp. 119-146.

[Color picker](https://www.w3schools.com/colors/colors_picker.asp)

**Daniel Schiffmann:**

[10 PRINT](https://www.youtube.com/watch?v=bEyTZ5ZZxZs)

[While and for loops](https://www.youtube.com/watch?v=cnRD9o6odjk&t=14s)
