/*rules
rule 1:
Everytime the color is blue, then it must turn either pink or purple.
If the color is pink or purple, it must turn blue

rule 2:
Everytime the color change the shape change depending on the color
*/

let state = 1; // state 1 = pink, state 2  = blue, state 3 = purple
let gridSpace = 20; 
let moveX = 0; //position on the x-axis
let moveY = 0; //position on the y-axis
let blueORpurple;


function setup(){
  createCanvas(windowWidth, windowHeight);
  background(255);
  grid(); //draw grid
  frameRate(40);
}



function draw() {
  colorChange();
  colorMove();
}


//Rule 1:
function colorChange(){
  if (state==2){
    blueORpurple = random(2);
    if (blueORpurple < 1){
      pinkRectangel();
    } else {
      purpelLine();
    }
  } else {
      blueLine();

  }
}


function colorMove(){
  moveX = moveX + gridSpace;
  //moveY = moveY+gridSpace
  if(moveX > width){
    moveX = 0;
    moveY = moveY+gridSpace;
  }
}

//rule 2
function pinkRectangel(){
  fill(255,51,204);
  noStroke();
  rect(moveX, moveY, gridSpace, gridSpace);
  state = 1;

}
//rule 2
function blueLine(){
  stroke(0, 0, 255); //blue
  line(moveX, moveY-gridSpace, moveX-gridSpace, moveY);
  state = 2;

}
//rule 2
function purpelLine(){
  stroke(204,0,255); //purple
  line(moveX, moveY, moveX+gridSpace, moveY+gridSpace);
  state = 3;

}

//draw grid
function grid(){

  for( let x = 0; x <= width; x += gridSpace){
    for (let y = 0; y <= height; y += gridSpace){
      stroke(0,50);
      strokeWeight(2);
      fill(255);
      rect(x, y, gridSpace, gridSpace);
    }
  }

}
